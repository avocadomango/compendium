package ro.safe.and.fun.agilesolution.gamification.reward;

import org.apache.commons.lang3.StringUtils;
import ro.safe.and.fun.agilesolution.compendium.model.Reward;
import ro.safe.and.fun.agilesolution.compendium.model.User;
import ro.safe.and.fun.agilesolution.compendium.model.Vote;

public class RewardProcessor {

    public static final String INITIAL_REWARD = "INITIAL_REWARD";
    public static final String INITIAL_REWARD_DESCRIPTION = "Initial reward granted to all users.";
    public static final String INITIAL_REWARD_TYPE = "INITITAL";
    public static final int BASIC_XP_REWARD = 5;
    public static final int DOUBLE_XP_REWARD = 10;
    public static final int DIMINISHED_XP_REWARD = 1;
    public static final int ENGAGEMENT_RATE_ADJUSTER = 1;
    public static final int THREAT_FACTOR_THRESHOLD = 50;
    public static final String SKIPPED_INTERACTION = "skip";

    public static Reward calculateReward(Vote vote) {
        int rewardScore = 0;
        int engagementRate = 100;
        int actualThreatFactor = getThreatFactorFromVote(vote);
        int estimatedThreatFactor = vote.getEstimatedThreatFactor();
        if (estimatedThreatFactor - actualThreatFactor < THREAT_FACTOR_THRESHOLD &&
            estimatedThreatFactor - actualThreatFactor > -THREAT_FACTOR_THRESHOLD) {
            rewardScore += DOUBLE_XP_REWARD;
        }
        if (StringUtils.isNumeric(vote.getThreatFactor()) && Integer.parseInt(vote.getThreatFactor()) > 0)
            rewardScore += BASIC_XP_REWARD;
        if (vote.getExploitabilityChamce().contains(SKIPPED_INTERACTION)) {
            rewardScore += DIMINISHED_XP_REWARD;
            engagementRate -= ENGAGEMENT_RATE_ADJUSTER;
        }
        else if (StringUtils.isNumeric(vote.getExploitabilityChamce()))
            rewardScore += BASIC_XP_REWARD;
        if (vote.getExploitabilityDamage().contains(SKIPPED_INTERACTION)) {
            rewardScore += DIMINISHED_XP_REWARD;
            engagementRate -= ENGAGEMENT_RATE_ADJUSTER;
        }
        else if (StringUtils.isNumeric(vote.getExploitabilityDamage()))
            rewardScore += BASIC_XP_REWARD;
        return new Reward(
                vote.getUser(), INITIAL_REWARD, INITIAL_REWARD_DESCRIPTION, INITIAL_REWARD_TYPE,
                engagementRate, rewardScore
        );
    }

    private static int getThreatFactorFromVote(Vote vote) {
        return StringUtils.isNumeric(vote.getThreatFactor())
                    ? Integer.parseInt(vote.getThreatFactor())
                    : 0;
    }

    public static Reward generateReward(Reward reward, User user) {
        return new Reward(user,
                reward.getName(),
                reward.getDescription(),
                reward.getType(),
                100,
                reward.getUserScore()
        );
    }

}
