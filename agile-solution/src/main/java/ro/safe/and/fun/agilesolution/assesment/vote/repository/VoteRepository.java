package ro.safe.and.fun.agilesolution.assesment.vote.repository;

import com.atlassian.jira.rest.client.api.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;
import ro.safe.and.fun.agilesolution.compendium.model.Vote;

import java.util.Optional;

public interface VoteRepository  extends JpaRepository<Vote, Long> {
    Optional<Vote> findByIssue(Issue issue);
    Optional<Vote> findByUser(User user);
}
