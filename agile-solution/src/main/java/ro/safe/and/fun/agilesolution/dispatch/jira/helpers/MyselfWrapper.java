package ro.safe.and.fun.agilesolution.dispatch.jira.helpers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MyselfWrapper {
    public String displayName;
    public String emailAddress;
    public String accountId;
    public Boolean active;
}
