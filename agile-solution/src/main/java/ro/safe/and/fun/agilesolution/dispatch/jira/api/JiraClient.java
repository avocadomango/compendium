package ro.safe.and.fun.agilesolution.dispatch.jira.api;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraDefaultCredentials;

import java.net.URI;

public class JiraClient {
    private JiraDefaultCredentials credentials = JiraDefaultCredentials.getInstance();
    private JiraRestClient restClient;

    public JiraClient() {
        this.restClient = getJiraRestClient();
    }

    private URI getJiraUri() {
        return URI.create(this.credentials.getJiraURL());
    }

    private JiraRestClient getJiraRestClient() {
        return new
            AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(
                        getJiraUri(),
                        this.credentials.getUsername(),
                        this.credentials.getPassword()
                );
    }

    public String createIssue(String projectKey, Long issueType, String issueSummary) {
        IssueRestClient issueClient = restClient.getIssueClient();
        IssueInput newIssue =
                new IssueInputBuilder(
                    projectKey,
                    issueType,
                    issueSummary
                ).build();
        return issueClient.createIssue(newIssue).claim().getKey();
    }

    public Issue getIssue(String issueKey) {
        return restClient.getIssueClient()
                .getIssue(issueKey)
                .claim();
    }
}
