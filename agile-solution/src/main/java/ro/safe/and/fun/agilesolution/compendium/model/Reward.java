package ro.safe.and.fun.agilesolution.compendium.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name = "reward")
public class Reward {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "reward_id")
    private long id;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonBackReference(value="user-reward")
    private User user;

    private String name;
    private String description;
    private String type;
    private int engagementRate;
    private int userScore;

    public Reward(User user, String name, String description, String type, int engagementRate, int userScore) {
//        this.id = 1232L;
        this.user = user;
        this.name = name;
        this.description = description;
        this.type = type;
        this.engagementRate = engagementRate;
        this.userScore = userScore;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getEngagementRate() {
        return engagementRate;
    }

    public void setEngagementRate(int engagementRate) {
        this.engagementRate = engagementRate;
    }

    public int getUserScore() {
        return userScore;
    }

    public void setUserScore(int userScore) {
        this.userScore = userScore;
    }

    public void adjustUserScore(int userScore) {
        this.userScore += userScore;
    }

    public void adjustEngagementRate(int engagementRate) {
        int engagementRateDifference = 100 - engagementRate;
        this.engagementRate -= engagementRateDifference;
    }
}
