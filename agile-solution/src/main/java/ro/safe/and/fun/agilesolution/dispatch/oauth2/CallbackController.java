package ro.safe.and.fun.agilesolution.dispatch.oauth2;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.RequiredArgsConstructor;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import ro.safe.and.fun.agilesolution.dispatch.jira.service.JiraService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequiredArgsConstructor
class CallbackController {

    @Autowired
    private JiraService jiraService;

    @GetMapping("/callback")
    String callback(HttpSession httpSession) {
        final SavedRequest savedRequest = (SavedRequest) httpSession.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        httpSession.removeAttribute("SPRING_SECURITY_SAVED_REQUEST");
        jiraService.getAccessToken();

        return "redirect:" + savedRequest.getRedirectUrl();
    }

    @GetMapping("/hello")
    String hello(HttpSession httpSession) {
        return "Indeed quite a hello";
    }
}