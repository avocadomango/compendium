package ro.safe.and.fun.agilesolution.compendium.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.safe.and.fun.agilesolution.compendium.model.User;

import java.util.Optional;

public interface UserRepository  extends JpaRepository<User, Long> {
    Optional<User> findByName(String name);
    Optional<User> findByToken(String token);
    Optional<User> findByEmail(String email);

}