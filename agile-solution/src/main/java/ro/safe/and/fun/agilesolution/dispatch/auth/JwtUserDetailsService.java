package ro.safe.and.fun.agilesolution.dispatch.auth;


import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.compendium.user.service.UserService;


@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (userService.hasValidOauthSession(username)) {
            Optional<ro.safe.and.fun.agilesolution.compendium.model.User> localUser =
                    userService.getUserByEmail(username);
            if (localUser.isPresent()) {
                return new User(
                        localUser.get().getEmail(),
                        "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
                        new ArrayList<>());
            } else {
                throw new UsernameNotFoundException("User not found with email: " + username);
            }
        } else {
            throw new UsernameNotFoundException("Please log in with Jira first");
        }
    }

}
