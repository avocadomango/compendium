package ro.safe.and.fun.agilesolution.compendium.user.service;

import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.compendium.model.User;

import java.util.Optional;

@Service
public interface UserService {

    Boolean hasValidOauthSession(String email);
    User saveOrUpdateUser(User user);
    Optional<User> getUserByEmail(String email);

}
