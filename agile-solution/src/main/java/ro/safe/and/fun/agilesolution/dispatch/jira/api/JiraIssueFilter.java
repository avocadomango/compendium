package ro.safe.and.fun.agilesolution.dispatch.jira.api;

import com.atlassian.jira.rest.client.api.domain.Issue;
import ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraConstants;

public class JiraIssueFilter {

    public static boolean isActiveIssue(Issue issue) {
        if (hasValidStatus(issue))
            return isNotFinalStatus(issue);
        return false;
    }

    public static boolean isNewIssue(Issue issue) {
        if (hasValidStatus(issue))
            return isTodoStatus(issue);
        return false;
    }

    private static boolean isTodoStatus(Issue issue) {
        return issue.getStatus().getName().equals(JiraConstants.TO_DO_STATUS);
    }

    private static boolean isNotFinalStatus(Issue issue) {
        return !issue.getStatus().getName().equals(JiraConstants.DONE_STATUS);
    }

    private static boolean hasValidStatus(Issue issue) {
        return issue.getStatus() != null && issue.getStatus().getName() != null;
    }
}
