package ro.safe.and.fun.agilesolution.dispatch.jira.helpers;

import ro.safe.and.fun.agilesolution.dispatch.jira.api.JiraIssue;

import java.util.ArrayList;
import java.util.List;

public class JiraIssuesWrapper {
    public String expand;
    public Integer startAt;
    public Integer maxResults;
    public Integer total;
    public List<JiraIssue> issues;

    public JiraIssuesWrapper() {
        issues = new ArrayList<>();
    }

    public String getExpand() {
        return expand;
    }

    public void setExpand(String expand) {
        this.expand = expand;
    }

    public Integer getStartAt() {
        return startAt;
    }

    public void setStartAt(Integer startAt) {
        this.startAt = startAt;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<JiraIssue> getIssues() {
        return issues;
    }

    public void setIssues(List<JiraIssue> issues) {
        this.issues = issues;
    }
}
