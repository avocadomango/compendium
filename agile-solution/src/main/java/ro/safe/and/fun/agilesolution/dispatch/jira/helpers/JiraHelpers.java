package ro.safe.and.fun.agilesolution.dispatch.jira.helpers;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ro.safe.and.fun.agilesolution.dispatch.jira.api.JiraIssue;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;

import java.util.ArrayList;
import java.util.List;

public class JiraHelpers {
    public static List<Issue> convertJiraIssuesToInteralIssues(List<JiraIssue> jiraIssues) {
        List<Issue> issues = new ArrayList<>();
        if (jiraIssues != null && !jiraIssues.isEmpty()) {
            for (JiraIssue jiraIssue : jiraIssues) {
                if (jiraIssue != null) {
                    issues.add(transformedJiraIssue(jiraIssue));
                }
            }
        }
        return issues;
    }

    public static Issue transformedJiraIssue(JiraIssue jiraIssue) {
        Issue issue =
                new Issue(
                        jiraIssue.getKey() != null ? jiraIssue.getKey() : "na",
                        jiraIssue.getSummary() != null ? jiraIssue.getSummary() : "na",
                        jiraIssue.getDescription() != null ? jiraIssue.getDescription() : "na",
                        jiraIssue.getPriority() != null ? jiraIssue.getPriority() : "na",
                        jiraIssue.getStatus() != null ? jiraIssue.getStatus() : "na",
                        jiraIssue.getThreatFactor() != null
                                ? jiraIssue.getThreatFactor().equals("null")
                                    ? "na" 
                                    : jiraIssue.getThreatFactor()
                                : "na"
                );
        return issue;
    }

    public static JiraIssuesWrapper generateIssueWrapperFromJson(String json) {
        JiraIssuesWrapper jiraIssuesWrapper = new JiraIssuesWrapper();
        try {
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module =
                    new SimpleModule(
                            "CustomCarDeserializer",
                            new Version(1, 0, 0, null, null, null)
                    );
            module.addDeserializer(
                    JiraIssuesWrapper.class,
                    new CustomJiraIssueDeserializer()
            );
            mapper.registerModule(module);
            jiraIssuesWrapper = mapper.readValue(json, JiraIssuesWrapper.class);
        } catch (Exception e) {

        }
        return jiraIssuesWrapper;
    }
}
