package ro.safe.and.fun.agilesolution.assesment.vote;

import ro.safe.and.fun.agilesolution.compendium.model.User;
import ro.safe.and.fun.agilesolution.compendium.model.Vote;

public class VoteProcessor {
    public static Vote transform(Vote vote, User user) {
            return new Vote(vote.getIssue(),
                    user, vote.isCanBeExploited(),
                    vote.getExploitabilityChamce(),
                    vote.getExploitabilityDamage(),
                    vote.getEffectOn(),
                    vote.getThreatFactor());

    }
}
