package ro.safe.and.fun.agilesolution.dispatch.oauth2;

import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.savedrequest.SimpleSavedRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class OAuth2RedirectStrategy extends DefaultRedirectStrategy {
    @Override
    public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {

        final String currentUri = (String) request.getAttribute("currentUri");
        final HttpSession session = request.getSession(false);
        if (session != null)
            session.setAttribute("SPRING_SECURITY_SAVED_REQUEST", new SimpleSavedRequest(currentUri));

        super.sendRedirect(request, response, url);
    }

}
