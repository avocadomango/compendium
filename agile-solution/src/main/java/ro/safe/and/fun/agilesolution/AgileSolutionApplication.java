package ro.safe.and.fun.agilesolution;

import com.atlassian.jira.rest.client.api.domain.Issue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.util.DefaultUriBuilderFactory;
import ro.safe.and.fun.agilesolution.dispatch.oauth2.OAuth2RedirectStrategy;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableOAuth2Client
public class AgileSolutionApplication {

	public static void main(String[] args) {

		SpringApplication.run(AgileSolutionApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE").allowedOrigins("*")
						.allowedHeaders("*");
			}
		};
	}

	@Bean
	@ConfigurationProperties("security.oauth2.client")
	OAuth2ProtectedResourceDetails oauth2RemoteResource() {
		List<String> scopes = new ArrayList<>();
		scopes.add("read:jira-work");
		scopes.add("read:jira-user");
		scopes.add("write:jira-work");
		AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
		details.setScope(scopes);
		return new AuthorizationCodeResourceDetails();

	}

	@Bean
	RedirectStrategy oAuth2RedirectStrategy() {
		return new OAuth2RedirectStrategy();
	}

	@Bean
	OAuth2RestTemplate oauth2RestTemplate(OAuth2ClientContext oauth2ClientContext, @Value("${app.jiraUrl}") String baseUrl) {
		final OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(oauth2RemoteResource(), oauth2ClientContext);
		restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(baseUrl));
		return restTemplate;
	}

	@Bean
	OAuth2ClientContextFilter oAuth2ClientContextFilter(OAuth2ClientContextFilter oAuth2ClientContextFilter) {
		oAuth2ClientContextFilter.setRedirectStrategy(oAuth2RedirectStrategy());
		return oAuth2ClientContextFilter;
	}
}
