package ro.safe.and.fun.agilesolution.dispatch.jira.helpers;

public class JiraConstants {
    public static String TO_DO_STATUS =  "To Do";
    public static String IN_PROGRESS_STATUS =  "In Progress";
    public static String DONE_STATUS =  "Done";

    public static String[] OPEN_STATUSES = { "To Do", "In Progress" };
    public static String[] CLOSED_STATUSES = { "Done", "Closed" };

    public static final String VOTING_GROUNDS_VIEW = "https://safe-and-fun-agile.ngrok.io/login";
    public static final String LOCAL_VOTING_GROUND = "http://localhost:4200";
    public static final String PRODUCTION_VOTING_GROUND = "https://the-fun-and-safe-agile.ngrok.io";

    public static final String JIRA_NOT_OK_MESSAGE = "NOT_OK";
    public static final String JIRA_OK_MESSAGE = "OK";

}
