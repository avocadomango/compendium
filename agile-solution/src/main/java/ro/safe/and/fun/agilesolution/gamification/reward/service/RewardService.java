package ro.safe.and.fun.agilesolution.gamification.reward.service;

import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.compendium.model.Reward;
import ro.safe.and.fun.agilesolution.compendium.model.User;

@Service
public interface RewardService {
    Reward saveOrUpdateReward(Reward reward, User user);
}
