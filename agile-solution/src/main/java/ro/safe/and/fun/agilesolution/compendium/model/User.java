package ro.safe.and.fun.agilesolution.compendium.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@NoArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "token", columnDefinition = "LONGTEXT")
    private String token;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy="user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value="user-votes")
    private Set<Vote> votes;

    @OneToOne(mappedBy = "user", cascade = CascadeType.MERGE, orphanRemoval = true)
    @JsonManagedReference(value="user-reward")
    private Reward reward;

    @Basic
    @Column(name = "tokenTimer")
    private java.time.LocalDateTime tokenTimer;

    @Column(name = "experiencePoints")
    private int experiencePoints;

    public User(String name) {
        this.name = name;
    }

    public User(String name, String token, String email, LocalDateTime tokenTimer) {
        this.name = name;
        this.token = token;
        this.email = email;
        this.tokenTimer = tokenTimer;
        this.experiencePoints = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Vote> getVotes() {
        return votes;
    }

    public void setVotes(Set<Vote> votes) {
        this.votes = votes;
    }

    public Reward getReward() {
        return reward;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public LocalDateTime getTokenTimer() {
        return tokenTimer;
    }

    public void setTokenTimer(LocalDateTime tokenTimer) {
        this.tokenTimer = tokenTimer;
    }

    public int getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(int experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public void addExperiencePoints(int experiencePoints) {
        this.experiencePoints += experiencePoints;
    }
}
