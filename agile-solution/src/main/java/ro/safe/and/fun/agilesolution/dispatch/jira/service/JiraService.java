package ro.safe.and.fun.agilesolution.dispatch.jira.service;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.dispatch.jira.api.JiraIssue;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;
import ro.safe.and.fun.agilesolution.dispatch.jira.helpers.MyselfWrapper;

import java.util.List;

@Service
public interface JiraService {

    public List<JiraIssue> getAllJiraIssues();
    public List<Issue> getAllIssues();
    public OAuth2AccessToken getAccessToken();
    public Object saveAllIssues(List<Issue> issues);
    public MyselfWrapper aboutMyself();
}
