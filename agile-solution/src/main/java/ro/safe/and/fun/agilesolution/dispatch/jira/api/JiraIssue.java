package ro.safe.and.fun.agilesolution.dispatch.jira.api;


import com.atlassian.jira.rest.client.api.ExpandableResource;
import com.atlassian.jira.rest.client.api.domain.*;
import com.atlassian.jira.rest.client.api.domain.util.UriUtil;
import com.google.common.base.MoreObjects.ToStringHelper;
import java.net.URI;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.annotation.Nullable;
import org.joda.time.DateTime;
import ro.safe.and.fun.agilesolution.dispatch.jira.api.JiraBasicIssue;

public class JiraIssue extends JiraBasicIssue implements ExpandableResource {
    private String status;
    private String priority;
    private String summary;
    @Nullable
    private String description;

    private IssueType issueType;
    private BasicProject project;
    private URI transitionsUri;
    private Iterable<String> expandos;
    private Collection<BasicComponent> components;
    @Nullable
    private User reporter;
    private User assignee;
    @Nullable
    private Resolution resolution;
    private Collection<IssueField> issueFields;
    private DateTime creationDate;
    private DateTime updateDate;
    private DateTime dueDate;
    private BasicVotes votes;
    @Nullable
    private Collection<Version> fixVersions;
    @Nullable
    private Collection<Version> affectedVersions;
    private Collection<Comment> comments;
    @Nullable
    private Collection<IssueLink> issueLinks;
    private Collection<Attachment> attachments;
    private Collection<Worklog> worklogs;
    private BasicWatchers watchers;
    @Nullable
    private TimeTracking timeTracking;
    @Nullable
    private Collection<Subtask> subtasks;
    @Nullable
    private Collection<ChangelogGroup> changelog;
    @Nullable
    private Operations operations;
    private Set<String> labels;

    @Nullable
    private String threatFactor;

    public JiraIssue(String summary, URI self, String key, Long id, BasicProject project, IssueType issueType, String status, String description, @Nullable String priority, @Nullable Resolution resolution, Collection<Attachment> attachments, @Nullable User reporter, @Nullable User assignee, DateTime creationDate, DateTime updateDate, DateTime dueDate, Collection<Version> affectedVersions, Collection<Version> fixVersions, Collection<BasicComponent> components, @Nullable TimeTracking timeTracking, Collection<IssueField> issueFields, Collection<Comment> comments, @Nullable URI transitionsUri, @Nullable Collection<IssueLink> issueLinks, BasicVotes votes, Collection<Worklog> worklogs, BasicWatchers watchers, Iterable<String> expandos, @Nullable Collection<Subtask> subtasks, @Nullable Collection<ChangelogGroup> changelog, @Nullable Operations operations, Set<String> labels) {
        super(self, key, id);
        this.summary = summary;
        this.project = project;
        this.status = status;
        this.description = description;
        this.resolution = resolution;
        this.expandos = expandos;
        this.comments = comments;
        this.attachments = attachments;
        this.issueFields = issueFields;
        this.issueType = issueType;
        this.reporter = reporter;
        this.assignee = assignee;
        this.creationDate = creationDate;
        this.updateDate = updateDate;
        this.dueDate = dueDate;
        this.transitionsUri = transitionsUri;
        this.issueLinks = issueLinks;
        this.votes = votes;
        this.worklogs = worklogs;
        this.watchers = watchers;
        this.fixVersions = fixVersions;
        this.affectedVersions = affectedVersions;
        this.components = components;
        this.priority = priority;
        this.timeTracking = timeTracking;
        this.subtasks = subtasks;
        this.changelog = changelog;
        this.operations = operations;
        this.labels = labels;
    }

    public JiraIssue() { }

    public String getStatus() {
        return this.status;
    }

    @Nullable
    public String getThreatFactor() {
        return threatFactor;
    }

    public void setThreatFactor(@Nullable String threatFactor) {
        this.threatFactor = threatFactor;
    }

    @Nullable
    public User getReporter() {
        return this.reporter;
    }

    @Nullable
    public User getAssignee() {
        return this.assignee;
    }

    public String getSummary() {
        return this.summary;
    }

    @Nullable
    public String getPriority() {
        return this.priority;
    }

    @Nullable
    public Iterable<IssueLink> getIssueLinks() {
        return this.issueLinks;
    }

    @Nullable
    public Iterable<Subtask> getSubtasks() {
        return this.subtasks;
    }

    public Iterable<IssueField> getFields() {
        return this.issueFields;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setIssueType(IssueType issueType) {
        this.issueType = issueType;
    }

    public void setProject(BasicProject project) {
        this.project = project;
    }

    public void setTransitionsUri(URI transitionsUri) {
        this.transitionsUri = transitionsUri;
    }

    public void setExpandos(Iterable<String> expandos) {
        this.expandos = expandos;
    }

    public void setComponents(Collection<BasicComponent> components) {
        this.components = components;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public void setReporter(@Nullable User reporter) {
        this.reporter = reporter;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public void setResolution(@Nullable Resolution resolution) {
        this.resolution = resolution;
    }

    public Collection<IssueField> getIssueFields() {
        return issueFields;
    }

    public void setIssueFields(Collection<IssueField> issueFields) {
        this.issueFields = issueFields;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public void setVotes(BasicVotes votes) {
        this.votes = votes;
    }

    public void setFixVersions(@Nullable Collection<Version> fixVersions) {
        this.fixVersions = fixVersions;
    }

    public void setAffectedVersions(@Nullable Collection<Version> affectedVersions) {
        this.affectedVersions = affectedVersions;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }

    public void setIssueLinks(@Nullable Collection<IssueLink> issueLinks) {
        this.issueLinks = issueLinks;
    }

    public void setAttachments(Collection<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void setWorklogs(Collection<Worklog> worklogs) {
        this.worklogs = worklogs;
    }

    public void setWatchers(BasicWatchers watchers) {
        this.watchers = watchers;
    }

    public void setTimeTracking(@Nullable TimeTracking timeTracking) {
        this.timeTracking = timeTracking;
    }

    public void setSubtasks(@Nullable Collection<Subtask> subtasks) {
        this.subtasks = subtasks;
    }

    public void setChangelog(@Nullable Collection<ChangelogGroup> changelog) {
        this.changelog = changelog;
    }

    public void setOperations(@Nullable Operations operations) {
        this.operations = operations;
    }

    public void setLabels(Set<String> labels) {
        this.labels = labels;
    }

    @Nullable
    public IssueField getField(String id) {
        Iterator var2 = this.issueFields.iterator();

        IssueField issueField;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            issueField = (IssueField)var2.next();
        } while(!issueField.getId().equals(id));

        return issueField;
    }

    @Nullable
    public IssueField getFieldByName(String name) {
        Iterator var2 = this.issueFields.iterator();

        IssueField issueField;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            issueField = (IssueField)var2.next();
        } while(!issueField.getName().equals(name));

        return issueField;
    }

    public Iterable<String> getExpandos() {
        return this.expandos;
    }

    public IssueType getIssueType() {
        return this.issueType;
    }

    public Iterable<Attachment> getAttachments() {
        return this.attachments;
    }

    public URI getAttachmentsUri() {
        return UriUtil.path(this.getSelf(), "attachments");
    }

    public URI getWorklogUri() {
        return UriUtil.path(this.getSelf(), "worklog");
    }

    public Iterable<Comment> getComments() {
        return this.comments;
    }

    public URI getCommentsUri() {
        return UriUtil.path(this.getSelf(), "comment");
    }

    public BasicProject getProject() {
        return this.project;
    }

    @Nullable
    public BasicVotes getVotes() {
        return this.votes;
    }

    public Iterable<Worklog> getWorklogs() {
        return this.worklogs;
    }

    @Nullable
    public BasicWatchers getWatchers() {
        return this.watchers;
    }

    @Nullable
    public Iterable<Version> getFixVersions() {
        return this.fixVersions;
    }

    @Nullable
    public URI getTransitionsUri() {
        return this.transitionsUri;
    }

    @Nullable
    public Iterable<Version> getAffectedVersions() {
        return this.affectedVersions;
    }

    public Iterable<BasicComponent> getComponents() {
        return this.components;
    }

    public Set<String> getLabels() {
        return this.labels;
    }

    @Nullable
    public Iterable<ChangelogGroup> getChangelog() {
        return this.changelog;
    }

    @Nullable
    public Operations getOperations() {
        return this.operations;
    }

    public URI getVotesUri() {
        return UriUtil.path(this.getSelf(), "votes");
    }

    @Nullable
    public Resolution getResolution() {
        return this.resolution;
    }

    public DateTime getCreationDate() {
        return this.creationDate;
    }

    public DateTime getUpdateDate() {
        return this.updateDate;
    }

    public DateTime getDueDate() {
        return this.dueDate;
    }

    @Nullable
    public TimeTracking getTimeTracking() {
        return this.timeTracking;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    protected ToStringHelper getToStringHelper() {
        return super.getToStringHelper().add("project", this.project).add("status", this.status).add("description", this.description).add("expandos", this.expandos).add("resolution", this.resolution).add("reporter", this.reporter).add("assignee", this.assignee).addValue("\n").add("fields", this.issueFields).addValue("\n").add("affectedVersions", this.affectedVersions).addValue("\n").add("fixVersions", this.fixVersions).addValue("\n").add("components", this.components).addValue("\n").add("issueType", this.issueType).add("creationDate", this.creationDate).add("updateDate", this.updateDate).addValue("\n").add("dueDate", this.dueDate).addValue("\n").add("attachments", this.attachments).addValue("\n").add("comments", this.comments).addValue("\n").add("transitionsUri", this.transitionsUri).add("issueLinks", this.issueLinks).addValue("\n").add("votes", this.votes).addValue("\n").add("worklogs", this.worklogs).addValue("\n").add("watchers", this.watchers).add("timeTracking", this.timeTracking).add("changelog", this.changelog).add("operations", this.operations).add("labels", this.labels);
    }
}

