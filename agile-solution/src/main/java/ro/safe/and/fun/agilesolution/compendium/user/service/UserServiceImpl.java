package ro.safe.and.fun.agilesolution.compendium.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.compendium.model.User;
import ro.safe.and.fun.agilesolution.compendium.user.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public Boolean hasValidOauthSession(String email) {
        Optional<User> user = userRepository.findByEmail(email);
        return user.isPresent() &&
                user.get().getTokenTimer().plusHours(3).isAfter(LocalDateTime.now());
    }

    @Override
    public User saveOrUpdateUser(User user) {
        Optional<User> dbUser = userRepository.findByEmail(user.getEmail());
        dbUser.ifPresent(
                value -> value.setTokenTimer(user.getTokenTimer())
        );
        return userRepository.save(dbUser.orElse(new User(user.getName(),user.getToken(), user.getEmail(), LocalDateTime.now())));
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
