package ro.safe.and.fun.agilesolution.compendium.issue.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;
import ro.safe.and.fun.agilesolution.compendium.issue.repository.IssueRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:8081", "http://localhost:4200"})
@Controller
@RequestMapping("/api")
public class IssueController {

    @Autowired
    IssueRepository issueRepository;

    @GetMapping("/issues")
    public Object getAllIssues(@RequestParam(required = false) String summary, WebRequest request) {
        List<Issue> issues = new ArrayList<Issue>();
        if (summary == null)
            issues.addAll(issueRepository.findAll());
        else
            issues.addAll(issueRepository.findBySummaryContaining(summary));
        if (issues.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(issues, HttpStatus.OK);
    }

    @GetMapping("/issues/{id}")
    public ResponseEntity<Issue> getIssueById(@PathVariable("id") long id) {
        Optional<Issue> issueData = issueRepository.findById(id);
        return issueData
                .map(issue -> new ResponseEntity<>(issue, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/issues/{identifier}")
    public ResponseEntity<Issue> getIssueById(@PathVariable("identifier") String identifier) {
        Optional<Issue> issueData = issueRepository.findByIdentifier(identifier);
        return issueData
                .map(issue -> new ResponseEntity<>(issue, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/issues")
    public ResponseEntity<Issue> createIssue(@RequestBody Issue issue) {
        try {
            Issue _issue = issueRepository.save(
                    new Issue(issue.getIdentifier(),
                            issue.getSummary(),
                            issue.getDescription(),
                            issue.getPriority(),
                            issue.getStatus(),
                            issue.getThreatFactor()));
            return new ResponseEntity<>(_issue, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/issues/{id}")
    public ResponseEntity<Issue> updateIssue(@PathVariable("id") long id, @RequestBody Issue issue) {
        Optional<Issue> issueData = issueRepository.findById(id);
        if (issueData.isPresent()) {
            Issue _issue = issueData.get();
            _issue.setSummary(issue.getSummary());
            _issue.setDescription(issue.getDescription());
            _issue.setIdentifier(issue.getIdentifier());
            _issue.setPriority(issue.getPriority());
            _issue.setStatus(issue.getStatus());
            return new ResponseEntity<>(issueRepository.save(_issue), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/issues/{id}")
    public ResponseEntity<HttpStatus> deleteIssue(@PathVariable("id") long id) {
        try {
            issueRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/issues")
    public ResponseEntity<HttpStatus> deleteAllIssues() {
        try {
            issueRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/issues/{priority}")
    public ResponseEntity<List<Issue>> findByPriority(@PathVariable("priority") String priority) {
        try {
            List<Issue> issues = issueRepository.findByPriority(priority);
            if (issues.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(issues, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
