package ro.safe.and.fun.agilesolution.compendium;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;
import ro.safe.and.fun.agilesolution.dispatch.jira.service.JiraService;
import javax.ws.rs.Produces;
import java.util.List;

import static ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraConstants.*;

@CrossOrigin(origins = {LOCAL_VOTING_GROUND, PRODUCTION_VOTING_GROUND})
@Controller
@RequiredArgsConstructor
@RequestMapping("/sync")
class SyncController {

    @Autowired
    private JiraService jiraService;

    @GetMapping
    @Produces({"text/html", "application/json"})
    Object sync(WebRequest request) {
        List<Issue> issuesX = jiraService.getAllIssues();
        String status =
                issuesX != null && !issuesX.isEmpty()
                    ? (String) jiraService.saveAllIssues(issuesX)
                    : JIRA_NOT_OK_MESSAGE;
        // todo only redirect to front end on OK status, redirect to error page if not

        return new RedirectView(VOTING_GROUNDS_VIEW);
    }

}