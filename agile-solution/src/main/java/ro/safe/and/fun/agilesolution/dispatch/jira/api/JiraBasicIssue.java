package ro.safe.and.fun.agilesolution.dispatch.jira.api;

import com.atlassian.jira.rest.client.api.AddressableEntity;
import com.atlassian.jira.rest.client.api.IdentifiableEntity;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.MoreObjects.ToStringHelper;
import java.net.URI;

public class JiraBasicIssue implements AddressableEntity, IdentifiableEntity<Long> {
    private URI self;
    private String key;
    private Long id;

    public JiraBasicIssue(URI self, String key, Long id) {
        this.self = self;
        this.key = key;
        this.id = id;
    }

    public JiraBasicIssue() { }

    public URI getSelf() {
        return this.self;
    }

    public String getKey() {
        return this.key;
    }

    public Long getId() {
        return this.id;
    }

    public void setSelf(URI self) {
        this.self = self;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String toString() {
        return this.getToStringHelper().toString();
    }

    protected ToStringHelper getToStringHelper() {
        return MoreObjects.toStringHelper(this).add("self", this.self).add("key", this.key).add("id", this.id);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof JiraBasicIssue)) {
            return false;
        } else {
            JiraBasicIssue that = (JiraBasicIssue)obj;
            return Objects.equal(this.self, that.self) && Objects.equal(this.key, that.key) && Objects.equal(this.id, that.id);
        }
    }

    public int hashCode() {
        return Objects.hashCode(new Object[]{this.self, this.key, this.id});
    }
}