package ro.safe.and.fun.agilesolution.compendium.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "issue")
public class Issue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "issue_id")
    private long id;

    @Column(name = "identifier")
    private String identifier;

    @Column(name = "summary")
    private String summary;

    @Column(name = "description")
    private String description;

    @Column(name = "priority")
    private String priority;

    @Column(name = "status")
    private String status;

    @OneToMany(mappedBy="issue", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value="issue-votes")
    private Set<Vote> votes;

    @Column(name = "threat_factor")
    private String threatFactor;

    public Issue() {  }

    public Issue(String identifier, String summary, String description, String priority, String status, String threatFactor) {
        this.identifier = identifier;
        this.summary = summary;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.threatFactor = threatFactor;
    }

    public String getThreatFactor() {
        return threatFactor;
    }

    public void setThreatFactor(String threatFactor) {
        this.threatFactor = threatFactor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Vote> getVotes() {
        return votes;
    }

    public void setVotes(Set<Vote> votes) {
        this.votes = votes;
    }
}
