package ro.safe.and.fun.agilesolution.gamification.reward.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.compendium.model.Reward;
import ro.safe.and.fun.agilesolution.compendium.model.User;
import ro.safe.and.fun.agilesolution.compendium.user.repository.UserRepository;
import ro.safe.and.fun.agilesolution.compendium.user.service.UserService;
import ro.safe.and.fun.agilesolution.gamification.reward.RewardProcessor;
import ro.safe.and.fun.agilesolution.gamification.reward.repository.RewardRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class RewardServiceImpl implements RewardService {

    @Autowired
    private RewardRepository rewardRepository;

    @Autowired
    private UserService userService;

    @Transactional
    @Override
    public Reward saveOrUpdateReward(Reward reward, User user) {
        Optional<Reward> dbReward = rewardRepository.findByUser(user);
        dbReward.ifPresent(
                value -> {
                    value.adjustEngagementRate(reward.getEngagementRate());
                    value.adjustUserScore(reward.getUserScore());
                }
        );
//        user.setReward(dbReward.orElse(reward));
//        userService.saveOrUpdateUser(user);
//
        Reward actualReward = dbReward.orElse(reward);

        user.setExperiencePoints(actualReward.getUserScore());
        userService.saveOrUpdateUser(user);

        return rewardRepository.save(
                dbReward
                        .orElse(RewardProcessor.generateReward(reward, user))
        );
    }
}
