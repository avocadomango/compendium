package ro.safe.and.fun.agilesolution.dispatch.jira.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import ro.safe.and.fun.agilesolution.compendium.model.User;
import ro.safe.and.fun.agilesolution.compendium.user.repository.UserRepository;
import ro.safe.and.fun.agilesolution.compendium.user.service.UserService;
import ro.safe.and.fun.agilesolution.dispatch.jira.api.JiraIssue;
import ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraIssuesWrapper;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;
import ro.safe.and.fun.agilesolution.compendium.issue.repository.IssueRepository;
import ro.safe.and.fun.agilesolution.dispatch.jira.helpers.MyselfWrapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraConstants.JIRA_OK_MESSAGE;
import static ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraHelpers.convertJiraIssuesToInteralIssues;
import static ro.safe.and.fun.agilesolution.dispatch.jira.helpers.JiraHelpers.generateIssueWrapperFromJson;

@Service
public class JiraServiceImpl implements JiraService {

    @Autowired
    private OAuth2RestTemplate restTemplate;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<JiraIssue> getAllJiraIssues() {
        JiraIssuesWrapper jiraIssuesWrapper =
            generateIssueWrapperFromJson(
                restTemplate.getForObject("/search?jql=project=\"TG\"", String.class)
            );
        return jiraIssuesWrapper.getIssues();
    }

    @Override
    public List<Issue> getAllIssues() {
        aboutMyself();
        return convertJiraIssuesToInteralIssues(getAllJiraIssues());
    }

    @Override
    public OAuth2AccessToken getAccessToken() {
        return restTemplate.getAccessToken();
    }

    @Override
    public Object saveAllIssues(List<Issue> issues) {
        for (Issue issue : issues) {
            Optional<Issue> dbIssue = issueRepository.findByIdentifier(issue.getIdentifier());
            issueRepository.save(dbIssue
                    .orElse(new Issue(issue.getIdentifier(), issue.getSummary(), issue.getDescription(), issue.getPriority(), issue.getStatus(), issue.getThreatFactor())));
        }
//        issueRepository.saveAll(issues);
        return JIRA_OK_MESSAGE;
    }

    @Override
    public MyselfWrapper aboutMyself() {
        MyselfWrapper myself =
            generateMyselfWrapperFromJson(
                restTemplate.getForObject("/myself", String.class)
        );

        if (myself.displayName != null && myself.active != null && myself.emailAddress != null) {
            userService.saveOrUpdateUser(
                    new User(
                            myself.displayName,
                            "token",
                            myself.emailAddress,
                            LocalDateTime.now())
            );
        }

        return myself;
    }

    private MyselfWrapper generateMyselfWrapperFromJson(String forObject) {
        ObjectMapper mapper = new ObjectMapper();
        MyselfWrapper wrapper = null;
        Map mapX = restTemplate.getForObject("/search?username=%25&maxResults", Map.class);
        try {
            wrapper = mapper.readValue(forObject, MyselfWrapper.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return wrapper;
    }

}
