package ro.safe.and.fun.agilesolution.compendium.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name = "votes")
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "vote_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "issue_id", nullable = false)
    @JsonBackReference(value="issue-votes")
    private Issue issue;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonBackReference(value="user-votes")
    private User user;

    private boolean canBeExploited;

    private String exploitabilityChamce;

    private String exploitabilityDamage;

    private String effectOn;

    private String threatFactor;

    public Vote(Issue issue, User user, boolean canBeExploited, String exploitabilityChamce, String exploitabilityDamage, String effectOn, String threatFactor) {
        this.issue = issue;
        this.user = user;
        this.canBeExploited = canBeExploited;
        this.exploitabilityChamce = exploitabilityChamce;
        this.exploitabilityDamage = exploitabilityDamage;
        this.effectOn = effectOn;
        this.threatFactor = threatFactor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isCanBeExploited() {
        return canBeExploited;
    }

    public void setCanBeExploited(boolean canBeExploited) {
        this.canBeExploited = canBeExploited;
    }

    public String getExploitabilityChamce() {
        return exploitabilityChamce;
    }

    public void setExploitabilityChamce(String exploitabilityChamce) {
        this.exploitabilityChamce = exploitabilityChamce;
    }

    public String getExploitabilityDamage() {
        return exploitabilityDamage;
    }

    public void setExploitabilityDamage(String exploitabilityDamage) {
        this.exploitabilityDamage = exploitabilityDamage;
    }

    public String getEffectOn() {
        return effectOn;
    }

    public void setEffectOn(String effectOn) {
        this.effectOn = effectOn;
    }

    public String getThreatFactor() {
        return threatFactor;
    }

    public int getEstimatedThreatFactor() {
        int estimatedThreatFactor = 0;
        if (StringUtils.isNumeric(this.exploitabilityChamce)) {
            estimatedThreatFactor += Integer.parseInt(this.exploitabilityChamce);
        } else if (this.exploitabilityChamce.contains("skip")) {
            estimatedThreatFactor += 15;
        }
        if (StringUtils.isNumeric(this.exploitabilityDamage)) {
            estimatedThreatFactor += Integer.parseInt(this.exploitabilityDamage);
        } else if (this.exploitabilityDamage.contains("skip")) {
            estimatedThreatFactor += 15;
        }
        if (this.effectOn.split(" ").length > 0) {
            estimatedThreatFactor += (this.effectOn.split(" ").length * 5);
        }
        return estimatedThreatFactor;
    }

    public void setThreatFactor(String threatFactor) {
        this.threatFactor = threatFactor;
    }
}
