package ro.safe.and.fun.agilesolution.compendium.issue.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;

import java.util.List;
import java.util.Optional;

public interface IssueRepository extends JpaRepository<Issue, Long> {
    Optional<Issue> findByIdentifier(String identifier);
    List<Issue> findByPriority(String priority);
    List<Issue> findBySummaryContaining(String summary);
    List<Issue> findByDescriptionContaining(String description);
}