package ro.safe.and.fun.agilesolution.assesment.vote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ro.safe.and.fun.agilesolution.assesment.vote.VoteProcessor;
import ro.safe.and.fun.agilesolution.assesment.vote.repository.VoteRepository;
import ro.safe.and.fun.agilesolution.compendium.issue.repository.IssueRepository;
import ro.safe.and.fun.agilesolution.compendium.model.Issue;
import ro.safe.and.fun.agilesolution.compendium.model.User;
import ro.safe.and.fun.agilesolution.compendium.model.Vote;
import ro.safe.and.fun.agilesolution.compendium.user.service.UserService;
import ro.safe.and.fun.agilesolution.gamification.reward.RewardProcessor;
import ro.safe.and.fun.agilesolution.gamification.reward.service.RewardService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:8081", "http://localhost:4200"})
@Controller
@RequestMapping("/api")
public class VoteController {

    public static final String OAUTH_SESSION_EXPIRED = "Your OAuth Session has expired. Pleas sync with Atlassian.";
    @Autowired
    VoteRepository voteRepository;

    @Autowired
    IssueRepository issueRepository;

    @Autowired
    UserService userService;

    @Autowired
    RewardService rewardService;

    @GetMapping("/votes")
    public Object getAllVotes(@RequestParam(required = false) WebRequest request) {
        List<Vote> votes = new ArrayList<Vote>(voteRepository.findAll());
        if (votes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(votes, HttpStatus.OK);
    }

    @GetMapping("/votes/{id}")
    public ResponseEntity<Vote> getVoteById(@PathVariable("id") long id) {
        Optional<Vote> voteData = voteRepository.findById(id);
        return voteData
                .map(vote -> new ResponseEntity<>(vote, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/votes/{issueIdentifier}")
    public ResponseEntity<Vote> getVoteByName(@PathVariable("issueIdentifier") String issueIdentifier) {
        Optional<Issue> issue = issueRepository.findByIdentifier(issueIdentifier);
        Optional<Vote> voteData =
                issue.isPresent()
                        ? voteRepository.findByIssue(issue.get())
                        : Optional.empty();
        return voteData
                .map(vote -> new ResponseEntity<>(vote, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @PostMapping("/votes")
    public ResponseEntity<Object> createVote(@RequestBody Vote vote) {
        try {
            Optional<User> user = userService.getUserByEmail(vote.getUser().getEmail());
            if (user.isPresent() && userService.hasValidOauthSession(vote.getUser().getEmail())) {
                Vote _vote = voteRepository.save(
                        VoteProcessor.transform(vote, user.get())
                );
                rewardService.saveOrUpdateReward(
                        RewardProcessor.calculateReward(vote), user.get()
                );
                return new ResponseEntity<>(_vote, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(OAUTH_SESSION_EXPIRED, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/votes/{id}")
    public ResponseEntity<HttpStatus> deleteVote(@PathVariable("id") long id) {
        try {
            voteRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/votes")
    public ResponseEntity<HttpStatus> deleteAllVotes() {
        try {
            voteRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
