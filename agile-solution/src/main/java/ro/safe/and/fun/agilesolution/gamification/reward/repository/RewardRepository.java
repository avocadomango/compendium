package ro.safe.and.fun.agilesolution.gamification.reward.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.safe.and.fun.agilesolution.compendium.model.Reward;
import ro.safe.and.fun.agilesolution.compendium.model.User;

import java.util.Optional;

public interface RewardRepository extends JpaRepository<Reward, Long> {
    Optional<Reward> findByName(String name);
    Optional<Reward> findByUser(User user);
}
