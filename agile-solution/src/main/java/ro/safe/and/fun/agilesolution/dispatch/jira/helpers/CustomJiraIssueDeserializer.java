package ro.safe.and.fun.agilesolution.dispatch.jira.helpers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ro.safe.and.fun.agilesolution.dispatch.jira.api.JiraIssue;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomJiraIssueDeserializer extends StdDeserializer<JiraIssuesWrapper> {

    public CustomJiraIssueDeserializer() {
        this(null);
    }

    public CustomJiraIssueDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public JiraIssuesWrapper deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException {
        JiraIssuesWrapper wrapper = new JiraIssuesWrapper();
        List<JiraIssue> issueList = new ArrayList<>();
        ObjectCodec codec = parser.getCodec();
        JsonNode node = codec.readTree(parser);
        //todo startAt, maxResults, total and maybe expand

        JsonNode issuesMasterNode = node.get("issues");
        if (issuesMasterNode != null) {
            for (int i = 0; i < issuesMasterNode.size(); i++) {
                JiraIssue jiraIssue = new JiraIssue();
                JsonNode issueNode = issuesMasterNode.get(i);
                if (issueNode != null) {
                    if (issueNode.get("id") != null) {
                        jiraIssue.setId(issueNode.get("id").asLong());
                    }
                    if (issueNode.get("self") != null) {
                        jiraIssue.setSelf(UriBuilder.fromUri(issueNode.get("self").asText()).build());
                    }
                    if (issueNode.get("key") != null) {
                        jiraIssue.setKey(issueNode.get("key").asText());
                    }
                    if (issueNode.get("fields") != null) {
                        JsonNode issueDetails = issueNode.get("fields");
                        if (issueDetails.get("summary") != null) {
                            jiraIssue.setSummary(issueDetails.get("summary").asText());
                        }
                        if (issueDetails.get("status") != null) {
                            if (issueDetails.get("status").get("name") != null)
                                jiraIssue.setStatus(issueDetails.get("status").get("name").asText());
                        }
                        if (issueDetails.get("priority") != null) {
                            if (issueDetails.get("priority").get("name") != null)
                                jiraIssue.setPriority(issueDetails.get("priority").get("name").asText());
                        }
                        if (issueDetails.get("customfield_10029") != null) {
                            jiraIssue.setThreatFactor(issueDetails.get("customfield_10029").asText());
                        }
                        if (issueDetails.get("description") != null) {
                            StringBuilder descriptionBuilder = new StringBuilder();
                            if (issueDetails.get("description").get("content") != null) {
                                JsonNode issueDescriptionParagrapth = issueDetails.get("description").get("content");
                                for (int j = 0; j < issueDescriptionParagrapth.size(); j++) {
                                    if (issueDescriptionParagrapth.get(j) != null) {
                                        if (issueDescriptionParagrapth.get(j).get("content") != null) {
                                            JsonNode issueDescriptionParagraphContent = issueDescriptionParagrapth.get(j).get("content");
                                            for (int k = 0; k < issueDescriptionParagraphContent.size(); k++) {
                                                if (issueDescriptionParagraphContent.get(k) != null) {
                                                    if (issueDescriptionParagraphContent.get(k).get("type") != null) {
                                                        String issueTextType = issueDescriptionParagraphContent.get(k).get("type").asText();
                                                        if (issueTextType.equals("text") && issueDescriptionParagraphContent.get(k).get("text") != null) {
                                                            descriptionBuilder.append(issueDescriptionParagraphContent.get(k).get("text").asText());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
//                                    descriptionBuilder.append("\n");
                                }
                                jiraIssue.setDescription(descriptionBuilder.toString());
                            }
                        }
                    }
                    issueList.add(jiraIssue);
                }
            }
        }
        wrapper.setIssues(issueList);
        return wrapper;
    }
}