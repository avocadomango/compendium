import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

export class User {
  constructor(public status: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient) {}
  authenticate(username: string) {
    return this.httpClient
      .post<any>("https://fun-and-safe-agile.ngrok.io/authenticate", { username })
      .pipe(
        map((userData: { token: string; }) => {
          localStorage.setItem("username", username);
          let tokenStr = "Bearer " + userData.token;
          localStorage.setItem("token", tokenStr);
          return userData;
        })
      );
  }

  isUserLoggedIn() {
    let user = localStorage.getItem("username");
    console.log(!(user === null));
    return !(user === null);
  }

  logOut() {
    localStorage.removeItem("username");
    localStorage.removeItem("token");
  }
}

