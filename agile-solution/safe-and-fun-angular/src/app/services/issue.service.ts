import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Issue} from "../models/issue.model";
import {Observable} from "rxjs";

// const baseUrl = 'http://localhost:8080/api/issues';
const baseUrl = 'https://fun-and-safe-agile.ngrok.io/api/issues';


@Injectable({
  providedIn: 'root'
})
export class IssueService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get<any>(baseUrl);
  }

  get(id: any): Observable<Issue> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findBySummary(summary: any): Observable<Issue[]> {
    return this.http.get<Issue[]>(`${baseUrl}?summary=${summary}`);
  }
}
