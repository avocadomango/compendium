import { TestBed } from '@angular/core/testing';

import { Vote.ServiceService } from './vote.service.service';

describe('Vote.ServiceService', () => {
  let service: Vote.ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Vote.ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
