import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {Issue} from "../models/issue.model";
import {User} from "../models/user.model";

const baseUrl = 'https://fun-and-safe-agile.ngrok.io/api/users';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  private shouldUpdateData = new BehaviorSubject<any>(false);
  shouldUpdate = this.shouldUpdateData.asObservable();

  constructor(private http: HttpClient) { }

  get(email: any): Observable<User> {
    return this.http.get(`${baseUrl}/${email}`);
  }

  triggerUpdate() {
    this.shouldUpdateData.next(true);
  }

}
