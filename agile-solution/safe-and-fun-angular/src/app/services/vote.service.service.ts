import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Vote} from "../models/vote.model";
import {Observable} from "rxjs";

// const baseUrl = 'http://localhost:8080/api/votes';
const baseUrl = 'https://fun-and-safe-agile.ngrok.io/api/votes';


@Injectable({
  providedIn: 'root'
})
export class VoteService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get<any>(baseUrl);
  }

  get(id: any): Observable<Vote> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findBySummary(summary: any): Observable<Vote[]> {
    return this.http.get<Vote[]>(`${baseUrl}?summary=${summary}`);
  }
}
