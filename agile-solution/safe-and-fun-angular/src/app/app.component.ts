import {Component} from '@angular/core';
import {AuthenticationService} from "./services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'safe-and-fun-angular';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) {

  }

  isLoggedIn() : boolean{
    return this.authenticationService.isUserLoggedIn();
  }

  logOut() {
    console.log('inside logout ???');
    this.authenticationService.logOut();
    this.router.navigate(['login']);
  }

}
