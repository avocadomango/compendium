export class Issue {
  constructor(){}
  id?: any;
  identifier?: string;
  summary?: string;
  description?: string;
  status?: string;
  priority?: string;
  threatFactor?: string;
}
