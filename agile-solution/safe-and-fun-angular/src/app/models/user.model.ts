export class User {
  email?: string;
  experiencePoints?: number;
  name?: string;
}
