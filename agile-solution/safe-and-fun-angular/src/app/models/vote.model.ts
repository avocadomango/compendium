import {Issue} from "./issue.model";
import {User} from "./user.model";

export class Vote {
  id?: string;
  issue?: Issue;
  user?: User;
  camBeExploited?: boolean;
  exploitabilityChamce?: string;
  exploitabilityDamage?: string;
  effectOn?: string;
  threatFactor?: string;
}
