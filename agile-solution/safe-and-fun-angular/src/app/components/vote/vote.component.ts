import {Component, Input, OnInit, Output, SimpleChanges, EventEmitter } from '@angular/core';
import {Vote} from "../../models/vote.model";
import {VoteService} from "../../services/vote.service.service";
import {Issue} from "../../models/issue.model";
import {User} from "../../models/user.model";
import {EventEmitterService} from "../../services/event-emitter.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})

export class VoteComponent implements OnInit {
  formatLabel(value: number) {
    // return value + '%';
    if(value >= 0 && value < 25)
      return "low";
    else if (value >= 25 && value < 50)
      return "medium";
    else if (value >= 50 && value < 75)
      return "high";
    else return "huge";
  }

  formatLabelDamage(value: number) {
    if(value >= 0 && value < 25)
      return "none";
    else if (value >= 25 && value < 50)
      return "little";
    else if (value >= 50 && value < 75)
      return "some";
    else return "a lot";
  }

  exploitabilityChanceNumber: number = 0;
  exploitabilityDamageNumber: number = 0;

  vote: Vote = {
    camBeExploited: false,
    effectOn: '',
    exploitabilityChamce: '',
    exploitabilityDamage: '',
    threatFactor: '',
    issue: new Issue()
  };
  isSubmited = false;
  @Input() issue: Issue | undefined;
  threatFactor: number | undefined;

  constructor(private voteService: VoteService,
              private userService: UserService,
              private eventEmitterService: EventEmitterService   ) {
  }

  ngOnInit(): void {
  }

  firstComponentFunction(){
    this.eventEmitterService.onFirstComponentButtonClick();
    console.log('it worked');
  }

  saveVote(): void {
    const data = {
      camBeExploited: this.vote.camBeExploited,
      effectOn: this.vote.effectOn != null ? this.vote.effectOn : 'skipped',
      user: { email: localStorage.getItem('username') },
      exploitabilityChamce: this.exploitabilityDamageNumber != 0 ? this.exploitabilityDamageNumber.toString() : 'skipped',
      exploitabilityDamage: this.exploitabilityChanceNumber != 0 ? this.exploitabilityChanceNumber.toString() : 'skipped',
      issue: this.issue,
      threatFactor: this.threatFactor != null ? this.threatFactor : 1
    };

    this.firstComponentFunction();
    this.voteService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.isSubmited = true;
          this.userService.triggerUpdate();
        },
        error => {
          console.log(error);
        });
  }

  newVote(): void {
    this.isSubmited = false;
    this.vote = {
      camBeExploited: false,
      effectOn: '',
      exploitabilityChamce: '',
      exploitabilityDamage: '',
      threatFactor: "",
      issue: new Issue()
    };
    this.exploitabilityChanceNumber = 0;
    this.exploitabilityDamageNumber = 0;

  }

  onReceivedIssue($event: Issue) {
    this.issue = $event;
  }

  getIssueIdentifier() {
    return this.vote.issue ? this.vote.issue.identifier : 'na'
  }

  skipExploitabilityChance() {
    this.exploitabilityChanceNumber = 0;
  }

  skipExploitabilityDamage() {
    this.exploitabilityDamageNumber = 0;
  }
}
