import {Component, OnInit} from '@angular/core';
import {IssueService} from "../../services/issue.service";
import {Issue} from "../../models/issue.model";

@Component({
  selector: 'app-add-issue',
  templateUrl: './add-issue.component.html',
  styleUrls: ['./add-issue.component.css']
})
export class AddIssueComponent implements OnInit {
  issue: Issue = {
    summary: '',
    description: '',
    priority: '',
    status: '',
    identifier: ''
  };
  submitted = false;

  constructor(private issueService: IssueService) {
  }

  ngOnInit(): void {
  }

  saveIssue(): void {
    const data = {
      summary: this.issue.summary,
      description: this.issue.description,
      status: this.issue.status,
      priority: this.issue.priority,
      identifier: this.issue.identifier
    };

    this.issueService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newIssue(): void {
    this.submitted = false;
    this.issue = {
      summary: '',
      description: '',
      priority: '',
      status: '',
      identifier: ''
    };
  }

}
