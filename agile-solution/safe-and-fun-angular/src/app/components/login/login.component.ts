import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  invalidLogin = false;

  @Input() error: string | null | undefined;

  constructor(private router: Router,
              private loginservice: AuthenticationService) { }

  ngOnInit() {
  }

  checkLogin() {
    (this.loginservice.authenticate(this.username).subscribe(
        data => {
          this.router.navigate(['']);
          this.invalidLogin = false
        },
        error => {
          this.invalidLogin = true;
          this.error = error.message;

        }
      )
    );

  }

}
