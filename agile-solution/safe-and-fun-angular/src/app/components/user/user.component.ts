import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {IssueService} from "../../services/issue.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../services/user.service";
import {EventEmitterService} from "../../services/event-emitter.service";
import {AuthenticationService} from "../../services/authentication.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  experience? = 0;
  name? = '';
  badge: string = '';
  // @ts-ignore
  subscription: Subscription;

  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private eventEmitterService: EventEmitterService,
              private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem("username")) {
      let user = localStorage.getItem("username");
      if (user) {
        this.getUser(user);
      }
    }
    if (this.eventEmitterService.subsVar==undefined) {
      this.eventEmitterService.subsVar = this.eventEmitterService.
      invokeFirstComponentFunction.subscribe((name:string) => {
        let user = localStorage.getItem("username");
        if (user) this.getUser(user);
      });
    }

    this.subscription = this.userService.shouldUpdate.subscribe(value => {
      if (value) {
        let user = localStorage.getItem("username");
        if (user) {
          this.getUser(user);
        }
      }
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getUser(email: string): void {
    this.userService.get(email)
      .subscribe(
        data => {
          this.experience = data.experiencePoints;
          this.name = data.name;
          this.badge = this.getBadgeFromExperiencePoints(data.experiencePoints);
          console.log(data);
        },
        error => {
          console.log(error);
          if (error.status == 401) {
            this.authenticationService.logOut();
            this.router.navigate(['login']);
          }
        });
  }

  private getBadgeFromExperiencePoints(experiencePoints: number | undefined) {
    if (experiencePoints) {
      if (experiencePoints > 100 && experiencePoints < 200) {
        return "<mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon>";
      } else if (experiencePoints > 200 && experiencePoints < 300) {
        return "<mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon>";
      } else if (experiencePoints > 300 && experiencePoints < 400) {
        return "<mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon>";
      } else if (experiencePoints > 400 && experiencePoints < 500) {
        return "<mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon>";
      } else if (experiencePoints > 500) {
        return "<mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon><mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon>";
      }
    } else {
      return "";
    }
    return "<mat-icon aria-hidden=\"false\" style=\"color:white;\">stars</mat-icon>";
  }
}
