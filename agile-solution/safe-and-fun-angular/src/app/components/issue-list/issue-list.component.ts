import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Issue} from "../../models/issue.model";
import {IssueService} from "../../services/issue.service";

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.css']
})
export class IssueListComponent implements OnInit {
  issues?: Issue[];
  currentIssue?: Issue | undefined;
  currentIndex = -1;
  summary = '';
  @Output() issue = new EventEmitter<Issue>();

  constructor(private issueService: IssueService) {
  }

  ngOnInit(): void {
    this.retrieveIssues();
  }

  retrieveIssues(): void {
    this.issueService.getAll()
      .subscribe(
        data => {
          this.issues = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveIssues();
    this.currentIssue = undefined;
    this.currentIndex = -1;
  }

  setActiveIssue(issue: Issue, index: number): void {
    this.currentIssue = issue;
    this.currentIndex = index;
    this.issue.emit(issue);
  }

  removeAllIssues(): void {
    this.issueService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchSummary(): void {
    this.issueService.findBySummary(this.summary)
      .subscribe(
        data => {
          this.issues = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  selectIssue(issue: Issue) {
    this.issue.emit(issue);
  }
}
