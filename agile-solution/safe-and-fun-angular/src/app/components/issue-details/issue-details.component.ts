import {Component, OnInit} from '@angular/core';
import {Issue} from "../../models/issue.model";
import {IssueService} from "../../services/issue.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-issue-details',
  templateUrl: './issue-details.component.html',
  styleUrls: ['./issue-details.component.css']
})
export class IssueDetailsComponent implements OnInit {
  currentIssue: Issue = {
    summary: '',
    description: '',
    status: '',
    priority: '',
    identifier: ''
  };
  message = '';

  constructor(
    private issueService: IssueService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.message = '';
    this.getIssue(this.route.snapshot.params.id);
  }

  getIssue(id: string): void {
    this.issueService.get(id)
      .subscribe(
        data => {
          this.currentIssue = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updatePriority(priority: string): void {
    const data = {
      summary: this.currentIssue.summary,
      description: this.currentIssue.description,
      identifier: this.currentIssue.identifier,
      status: this.currentIssue.status,
      priority: priority
    };

    this.issueService.update(this.currentIssue.id, data)
      .subscribe(
        response => {
          this.currentIssue.priority = priority;
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }

  updateIssue(): void {
    this.issueService.update(this.currentIssue.id, this.currentIssue)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }

  deleteIssue(): void {
    this.issueService.delete(this.currentIssue.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/issues']);
        },
        error => {
          console.log(error);
        });
  }

  votingSession() {

  }
}
