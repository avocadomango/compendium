import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IssueListComponent} from "./components/issue-list/issue-list.component";
import {IssueDetailsComponent} from "./components/issue-details/issue-details.component";
import {AddIssueComponent} from "./components/add-issue/add-issue.component";
import {VoteComponent} from "./components/vote/vote.component";
import {LoginComponent} from "./components/login/login.component";
import {LogoutComponent} from "./components/logout/logout.component";
import {AuthGaurdService} from "./services/auth-guard.service";

const routes: Routes = [
  {path: '', component:IssueListComponent , canActivate:[AuthGaurdService] },
  {path: 'issues', component: IssueListComponent, canActivate:[AuthGaurdService] },
  {path: 'issues/:id', component: IssueDetailsComponent, canActivate:[AuthGaurdService] },
  {path: 'add', component: AddIssueComponent, canActivate:[AuthGaurdService] },
  {path: 'vote', component: VoteComponent, canActivate:[AuthGaurdService] },
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent, canActivate:[AuthGaurdService] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
